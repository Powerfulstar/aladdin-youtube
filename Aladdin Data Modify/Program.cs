﻿using MyToolkit.Multimedia;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;



namespace Aladdin_Data_Modify
{
    class Program
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Season { get; set; }
        public string Summary { get; set; }
        public string Episode { get; set; }        
        public DateTime PubDate { get; set; }
        public Uri YoutubeLink { get; set; }        
        public Uri Thumbnail { get; set; }
        public string Watched { get; set; }
   
        static void Main(string[] args)
        {
            WebClient client = new WebClient();

            string id = "PLVoDzINI4LDXlyWD9MQVVAiavG8qUhMTk";
            string Url2 = @"http://gdata.youtube.com/feeds/base/playlists/" + id + @"?orderby=published&max-results=50&alt=rss";


            string response = client.DownloadString(@"http://json.fivehand.net/");
            Console.WriteLine(response);
            
                StringReader stringReader = new StringReader(response);
                System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(stringReader);
                SyndicationFeed feed = SyndicationFeed.Load(xmlReader);
                Program video;
                List<Program> AllVideos = new List<Program>();




                foreach (SyndicationItem item in feed.Items)
                {
                    video = new Program();

                    video.YoutubeLink = item.Links[0].Uri;
                    string a = video.YoutubeLink.ToString().Remove(0, 31);
                    video.Id = a.Substring(0, 11);
                    video.Title = item.Title.Text;
                    video.PubDate = item.PublishDate.DateTime;
                    video.Thumbnail = YouTube.GetThumbnailUri(video.Id, YouTubeThumbnailSize.Large);

                    video.Season = "CBS Season 2 (1995)";                    
                    AllVideos.Add(video);
                }
                string json = JsonConvert.SerializeObject(AllVideos);
                File.WriteAllText(@"E:\some4.txt", json);
          
            Console.ReadLine();
        }
    }
}
