﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using Aladdin.ViewModel;
using System.Windows.Media;
using Microsoft.Phone.Tasks;
using Aladdin.Model;

namespace Aladdin.View
{
    public partial class MediaPlayer : PhoneApplicationPage
    {
        public MediaPlayer()
        {
            InitializeComponent();
            this.Loaded += CustomeMediaPlayerPage_Loaded;
        }
        DispatcherTimer timer = new DispatcherTimer();
        void CustomeMediaPlayerPage_Loaded(object sender, RoutedEventArgs e)
        {
            player.MediaOpened += player_MediaOpened;
            player.MediaEnded += player_MediaEnded;
            timer.Tick += timer_Tick;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            slider.Value = (double)player.Position.TotalMilliseconds;
            //buffertext.Text = player.BufferingProgress.ToString() + " - " + player.BufferingTime.ToString();

        }

        void player_MediaEnded(object sender, RoutedEventArgs e)
        {
            player.Stop();
            (App.Current.Resources["Locator"] as ViewModelLocator).Main.PlayButtonIconUri = @"/Toolkit.Content/refresh.png";
        }

        void player_MediaOpened(object sender, RoutedEventArgs e)
        {

            timer.Start();
            player.Play();
            var seconds = player.NaturalDuration.TimeSpan.Seconds % 60.00;

            durationValue.Text = player.NaturalDuration.TimeSpan.Minutes.ToString() + ":" + seconds.ToString();
            slider.Maximum = player.NaturalDuration.TimeSpan.TotalMilliseconds;
        }

        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (e.NewValue - e.OldValue > 300)
            {
                player.Position = new TimeSpan(0, 0, 0, 0, (int)slider.Value);
            }
            else if (e.OldValue - e.NewValue > 300)
            {
                player.Position = new TimeSpan(0, 0, 0, 0, (int)slider.Value);
            }
        }
        bool flag = true;
        private void player_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (flag)
            {
                down.Begin();
                flag = false;
            }
            else if (!flag)
            {
                up.Begin();
                flag = true;
            }

        }

        private void forward_Click(object sender, RoutedEventArgs e)
        {
            if (player.Position < player.NaturalDuration)
            {
                player.Position += new TimeSpan(0, 0, 0, 5);
            }
        }

        private void backward_Click(object sender, RoutedEventArgs e)
        {
            var time = new TimeSpan(0, 0, 0, 0);
            if (player.Position > time)
            {
                player.Position -= new TimeSpan(0, 0, 0, 5);
            }

        }

        private void playOrpause_Click(object sender, RoutedEventArgs e)
        {
            if (player.CurrentState == MediaElementState.Playing)
            {
                (App.Current.Resources["Locator"] as ViewModelLocator).Main.PlayButtonIconUri = @"/Toolkit.Content/transport.play.png";
                player.Pause();
            }
            else if (player.CurrentState == MediaElementState.Paused)
            {
                (App.Current.Resources["Locator"] as ViewModelLocator).Main.PlayButtonIconUri = @"/Toolkit.Content/transport.pause.png";
                player.Play();
            }
            else if (player.CurrentState == MediaElementState.Stopped)
            {
                (App.Current.Resources["Locator"] as ViewModelLocator).Main.PlayButtonIconUri = @"/Toolkit.Content/transport.pause.png";
                player.Play();
            }
            else if (player.CurrentState == MediaElementState.Closed)
            {
                (App.Current.Resources["Locator"] as ViewModelLocator).Main.PlayButtonIconUri = @"/Toolkit.Content/transport.pause.png";
                //player.Source = (App.Current.Resources["Locator"] as ViewModelLocator).Main.StreamingUrl;
                player.Source = new Uri(@"/SampleDaTa/videoplayback1.mp4", UriKind.Relative);
                player.Play();
            }
        }

        private void player_BufferingProgressChanged(object sender, RoutedEventArgs e)
        {
            if (player.BufferingProgress == 1)
            {
                (App.Current.Resources["Locator"] as ViewModelLocator).Main.DataLoadingOnMediaPlayer = false;
            }
            else if (player.BufferingProgress < 1)
            {
                (App.Current.Resources["Locator"] as ViewModelLocator).Main.DataLoadingOnMediaPlayer = true;
            }
        }

        private void downloadMenu_Click(object sender, EventArgs e)
        {

        }

        private void shareMenu_Click(object sender, EventArgs e)
        {
            ShareStatusTask shareStatusTask = new ShareStatusTask();
            Video video = (App.Current.Resources["Locator"] as ViewModelLocator).Main.SelectedVideo;
            shareStatusTask.Status = video.Title + "\n" + @"https://www.youtube.com/watch?v=" + video.Id;

            shareStatusTask.Show();
        }



    }
}