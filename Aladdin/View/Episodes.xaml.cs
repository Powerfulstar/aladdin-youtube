﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using GalaSoft.MvvmLight.Messaging;

namespace Aladdin.View
{
    public partial class Episodes : PhoneApplicationPage
    {
        public Episodes()
        {
            InitializeComponent();
            Messenger.Default.Register<Uri>(this, "Play", (uri) => NavigationService.Navigate(uri));
        }
    }
}