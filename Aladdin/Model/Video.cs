﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aladdin.Model
{
    public class Video
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Season { get; set; }
        public string Summary { get; set; }
        public string Episode { get; set; }
        public DateTime PubDate { get; set; }
        public Uri YoutubeLink { get; set; }
        public Uri Thumbnail { get; set; }
        public string Watched { get; set; }   
    }
}
