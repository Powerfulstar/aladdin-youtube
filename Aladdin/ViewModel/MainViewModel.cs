using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Linq;
using System.Collections.ObjectModel;
using Aladdin.Model;
using System.Net.Http;
using Newtonsoft.Json;
using MyToolkit.Multimedia;
namespace Aladdin.ViewModel
{
     
    public class MainViewModel : ViewModelBase
    {
        #region VideoList
        /// <summary>
        /// The <see cref="VideoList" /> property's name.
        /// </summary>
        public const string VideoListPropertyName = "VideoList";

        private ObservableCollection<Video> _videoList = new ObservableCollection<Video>();

        /// <summary>
        /// Sets and gets the VideoList property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<Video> VideoList
        {
            get
            {
                return _videoList;
            }

            set
            {
                if (_videoList == value)
                {
                    return;
                }

                RaisePropertyChanging(VideoListPropertyName);                
                _videoList = value;
                RaisePropertyChanged(VideoListPropertyName);
            }
        }
        #endregion
        #region Lists
        /// <summary>
        /// The <see cref="Series1" /> property's name.
        /// </summary>
        public const string Series1PropertyName = "Series1";

        private ObservableCollection<Video> _Series1 = new ObservableCollection<Video>();

        /// <summary>
        /// Sets and gets the Series1 property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<Video> Series1
        {
            get
            {
                return _Series1;
            }

            set
            {
                if (_Series1 == value)
                {
                    return;
                }

                RaisePropertyChanging(Series1PropertyName);
                _Series1 = value;
                RaisePropertyChanged(Series1PropertyName);
            }
        }
        /// <summary>
        /// The <see cref="Series2" /> property's name.
        /// </summary>
        public const string Series2PropertyName = "Series2";

        private ObservableCollection<Video> _Series2 = new ObservableCollection<Video>();

        /// <summary>
        /// Sets and gets the Series2 property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<Video> Series2
        {
            get
            {
                return _Series2;
            }

            set
            {
                if (_Series2 == value)
                {
                    return;
                }

                RaisePropertyChanging(Series2PropertyName);
                _Series2 = value;
                RaisePropertyChanged(Series2PropertyName);
            }
        }
        /// <summary>
        /// The <see cref="Series3" /> property's name.
        /// </summary>
        public const string Series3PropertyName = "Series3";

        private ObservableCollection<Video> _Series3 = new ObservableCollection<Video>();

        /// <summary>
        /// Sets and gets the Series3 property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<Video> Series3
        {
            get
            {
                return _Series3;
            }

            set
            {
                if (_Series3 == value)
                {
                    return;
                }

                RaisePropertyChanging(Series3PropertyName);
                _Series3 = value;
                RaisePropertyChanged(Series3PropertyName);
            }
        }
        #endregion
        #region SelectedVideo
        /// <summary>
        /// The <see cref="SelectedVideo" /> property's name.
        /// </summary>
        public const string SelectedVideoPropertyName = "SelectedVideo";

        private Video _SelectedVideo = new Video();

        /// <summary>
        /// Sets and gets the SelectedVideo property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Video SelectedVideo
        {
            get
            {
                return _SelectedVideo;
            }

            set
            {
                if (_SelectedVideo == value)
                {
                    return;
                }

                RaisePropertyChanging(SelectedVideoPropertyName);
                _SelectedVideo = value;
                RaisePropertyChanged(SelectedVideoPropertyName);
            }
        }
        #endregion
        #region VideoPlayCommand
        /// <summary>
        /// The <see cref="VideoPLayCommand" /> property's name.
        /// </summary>
        public const string VideoPLayCommandPropertyName = "VideoPLayCommand";

        private RelayCommand<object> _VideoPLayCommand = null;

        /// <summary>
        /// Sets and gets the VideoPLayCommand property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public RelayCommand<object> VideoPLayCommand
        {
            get
            {
                return _VideoPLayCommand;
            }

            set
            {
                if (_VideoPLayCommand == value)
                {
                    return;
                }

                RaisePropertyChanging(VideoPLayCommandPropertyName);
                _VideoPLayCommand = value;
                RaisePropertyChanged(VideoPLayCommandPropertyName);
            }
        }
        #endregion
        #region PlayButtonIconUri
        /// <summary>
        /// The <see cref="PlayButtonIconUri" /> property's name.
        /// </summary>
        public const string PlayButtonIconUriPropertyName = "PlayButtonIconUri";

        private string _PlayButtonIconUri = @"/Toolkit.Content/transport.pause.png";

        /// <summary>
        /// Sets and gets the PlayButtonIconUri property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string PlayButtonIconUri
        {
            get
            {
                return _PlayButtonIconUri;
            }

            set
            {
                if (_PlayButtonIconUri == value)
                {
                    return;
                }

                RaisePropertyChanging(PlayButtonIconUriPropertyName);
                _PlayButtonIconUri = value;
                RaisePropertyChanged(PlayButtonIconUriPropertyName);
            }
        }
        #endregion
        #region DataLoadingOnMediaPlayer
        /// <summary>
        /// The <see cref="DataLoadingOnMediaPlayer" /> property's name.
        /// </summary>
        public const string DataLoadingOnMediaPlayerPropertyName = "DataLoadingOnMediaPlayer";

        private bool _DataLoadingOnMediaPlayer = false;

        /// <summary>
        /// Sets and gets the DataLoadingOnMediaPlayer property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool DataLoadingOnMediaPlayer
        {
            get
            {
                return _DataLoadingOnMediaPlayer;
            }

            set
            {
                if (_DataLoadingOnMediaPlayer == value)
                {
                    return;
                }

                RaisePropertyChanging(DataLoadingOnMediaPlayerPropertyName);
                _DataLoadingOnMediaPlayer = value;
                RaisePropertyChanged(DataLoadingOnMediaPlayerPropertyName);
            }
        }
        #endregion
        #region StreamingUri
        /// <summary>
        /// The <see cref="StreamingUri" /> property's name.
        /// </summary>
        public const string StreamingUriPropertyName = "StreamingUri";

        private Uri _StreamingUri = null;

        /// <summary>
        /// Sets and gets the StreamingUri property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Uri StreamingUri
        {
            get
            {
                return _StreamingUri;
            }

            set
            {
                if (_StreamingUri == value)
                {
                    return;
                }

                RaisePropertyChanging(StreamingUriPropertyName);
                _StreamingUri = value;
                RaisePropertyChanged(StreamingUriPropertyName);
            }
        }
        #endregion
        public MainViewModel()
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
            LoadData();
            VideoPLayCommand = new RelayCommand<object>((param) => VideoPLaymethod(param));
        }

        private async void VideoPLaymethod(object param)
        {
            SelectedVideo = param as Video;
            Uri uri = new Uri("/View/MediaPlayer.xaml", UriKind.Relative);
            Messenger.Default.Send<Uri>(uri, "Play");

            var url = await YouTube.GetVideoUriAsync(SelectedVideo.Id, MyToolkit.Multimedia.YouTubeQuality.Quality480P);
            StreamingUri = url.Uri;
            
        }
        private async void LoadData()
        {
            HttpClient client = new HttpClient();
            string Json = await client.GetStringAsync(Constants.Constant.Url);

            VideoList = JsonConvert.DeserializeObject<ObservableCollection<Video>>(Json);

            foreach (var item in VideoList)
            {
                if (item.Season == "Disney Afternoon (1994�1995)")
                {
                    Series1.Add(item);
                }
                else if (item.Season == "CBS Season 1 (1994)")
                {
                    Series2.Add(item);
                }
                else if (item.Season == "CBS Season 2 (1995)")
                {
                    Series3.Add(item);
                }
            }
        }
    }
}